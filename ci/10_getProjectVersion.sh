#!/usr/bin/env bash
# this script will build into /usr/bin/app/build

# run with:
# buildJar.sh
# or
# buildJar.sh test build ...

set -x

export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )

## includes
. $SCRIPT_DIR/05_SHARED_LIB.sh



## enter main project dir
cd $PROJECT_DIR


## export branch name , ie: "master"
echo $(getCurrentBranch) > "${BRANCH_NAME_TMP_FILE}"

## if this commit have a tag assigned, export it to file ie: "v1.1.0"
echo $(getCurrentTag)  > "${TAG_NAME_TMP_FILE}"

## extract current version from file
echo $(getCurrentTag)  > "${VERSION_NAME_TMP_FILE}"