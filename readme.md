## hyperkube official image has been deprecated ,
https://github.com/kubernetes/kubernetes/tree/release-1.18/build
https://github.com/kubernetes/kubernetes/issues/81760



### latest available makefile for hyperkube 
( it's a k8s.gcr.io/build-image/debian-hyperkube-base-$(ARCH):v1.1.3  + binaries )
https://github.com/kubernetes/kubernetes/tree/release-1.18/cluster/images/hyperkube


## hyperkube image 
the hyperkube compiled file is taken from the original 
 `gcr.io/google_containers/hyperkube` image
 but because some libraries may be missing and also they are not enforcing compatibility
  ie : https://github.com/kubernetes/kubernetes/issues/55012
 I've made this image with also runtime libraries necessary for my usecases ( ie custom flexvolumes, .. )
    

git tags idemntify this docker-image tag versions and also hyperkube version too
 
base image is taken and enhanced from :
  https://github.com/dims/kubernetes/blob/master/build/debian-hyperkube-base/Dockerfile

## public repository source:


### manual build and push 
```shell
VERSION=v1.18.20
docker build -t "quay.io/fravi/vigo-hyperkube:$VERSION" .  

```
### local build & push

```bash
./ci/10_getProjectVersion.sh
## build only  -->  test/vigo-hyperkube:master
./ci/50_buildImage.sh
## build & push  --> quay.io/fravi/vigo-hyperkube:master or :$tag
DOCKER_REPO="quay.io/fravi" PUSH=1 IMAGENAME="vigo-hyperkube" DOCKER_BUILD_OPTS="--network=host" ./ci/50_buildImage.sh
```

### local build & push ( non-multistage version )


```bash
# prepare 
./ci-init/000-download-hyperkube-data.sh


./ci/10_getProjectVersion.sh
## build only  -->  test/vigo-hyperkube:master
./ci/50_buildImage.sh
## build & push  --> quay.io/fravi/vigo-hyperkube:master or :$tag
DOCKER_REPO="quay.io/fravi" PUSH=1 IMAGENAME="vigo-hyperkube" DOCKER_BUILD_OPTS="--network=host" ./ci/50_buildImage.sh

# clean 
./ci-init/999-clean-hyperkube-data.sh
```



### retag in case you want to overwrite specifig tag:
```bash
git push --delete origin tagname
git tag --delete tagname
```

### nb: 
  - this Dockerfile uses multi-stage builds which are not available in docker<17.06
    use manual build and upload describe here above if necessary 
